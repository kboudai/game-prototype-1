var Plane = function (x, y , z)
{
	this.geometry = new THREE.PlaneGeometry(cubeSize - 5 , cubeSize - 5 , 5, 5);
	this.material= new THREE.MeshLambertMaterial({color: this.mainColor}); // 0xc90d87
	this.plane = new THREE.Mesh(this.geometry, this.material);
	this.plane.position.x = x;
	this.plane.position.y = y;
	this.plane.position.z = z;
	return this.plane;
}


Plane.prototype.mainColor = 0x72401d;

Plane.prototype.highLightColor = 0xf8ff24;