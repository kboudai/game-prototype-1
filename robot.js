var Robot = function(x , y)
{
	this.cubeGeometry = new THREE.CubeGeometry(cubeSize/1.7,cubeSize/1.3,cubeSize/2);
	this.cubeMaterial = new THREE.MeshLambertMaterial( { color: 0x2d8f00 } );
	this.cubeMesh = new THREE.Mesh( this.cubeGeometry, this.cubeMaterial );

	var leftGeometry = new THREE.CubeGeometry(10, 10, 20);
	var leftMaterial = new THREE.MeshLambertMaterial( { color: 0x2d8f00 } );
	this.leftMesh = new THREE.Mesh( leftGeometry, leftMaterial );

	this.leftMesh.position.x -= (cubeSize/3.4)+ 6;
	this.leftMesh.position.y = 10;
	this.leftMesh.position.z = 20;

	this.leftMesh.rotation.x = -10;

	this.cubeMesh.add(this.leftMesh);

	rightGeometry = new THREE.CubeGeometry(10, 10, 20);
	rightMaterial = new THREE.MeshLambertMaterial( { color: 0x2d8f00 } );
	this.rightMesh = new THREE.Mesh( rightGeometry, rightMaterial );

	this.rightMesh.position.x = (cubeSize/3.4)+ 6;
	this.rightMesh.position.y = 10;
	this.rightMesh.position.z = 20;

	this.rightMesh.rotation.x = -10;

	this.cubeMesh.add(this.rightMesh);

	var sphereGeometry = new THREE.SphereGeometry(15, 15, 15);
	var sphereMaterial = new THREE.MeshPhongMaterial({color: 0x0000ff});
	this.sphereMesh = new THREE.Mesh(sphereGeometry, sphereMaterial);

	this.sphereMesh.position.z = cubeSize/2;

	this.cubeMesh.add(this.sphereMesh);

	var pos = game.gridToMesh( (x || 0), (y || 0) );

	this.cubeMesh.position.x = pos.x;
	this.cubeMesh.position.y = pos.y;
	this.cubeMesh.position.z = cubeSize/4;
}

Robot.prototype.move = function(dx, dy)
{
	var pos = game.meshToGrid(	this.cubeMesh.position.x,
								this.cubeMesh.position.y,
								this.cubeMesh.position.z);
	pos.x += (dx || 0);
	pos.y += (dy || 0);

	if ( pos.x >= 0 && pos.x <= 7 &&
		 pos.y >= 0 && pos.y <= 7 &&
		!(game.gameGrid[pos.x][pos.y] instanceof Block) )
	{
		this.cubeMesh.position.x += (dx*cubeSize || 0);
		this.cubeMesh.position.y += (dy*cubeSize || 0);
	}
	else if (pos.y > 7)
	{
		alert("Congratulations, you win! \n Dinosaur!");
		game.reInit(true);
	}
	
	// this.printPos();
}

Robot.prototype.explode = function()
{
	this.cubeMesh.position.z -= 1;
	this.sphereMesh.position.z += 10;
	this.leftMesh.position.x -= 1;
	this.rightMesh.position.x += 1;
}

Robot.prototype.printPos = function()
{
	console.log('Robot x: ' + this.cubeMesh.position.x +
				' y: ' + this.cubeMesh.position.y +
				' z: ' + this.cubeMesh.position.z);
}



