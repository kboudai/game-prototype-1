var Block = function(x , y)
{
	this.geometry = new THREE.CubeGeometry(cubeSize,cubeSize,cubeSize);
	this.material = new THREE.MeshLambertMaterial( { color: this.color } );
	this.mesh = new THREE.Mesh( this.geometry, this.material );

	this.meshPostion = game.gridToMesh(x, y);

	this.mesh.position.x = this.meshPostion.x || 0;
	this.mesh.position.y = this.meshPostion.y || 0;
	this.mesh.position.z = cubeSize/2;
}

Block.prototype.shiftRight = function()
{
	this.meshPostion = game.meshToGrid(	this.mesh.position.x+cubeSize,
										this.mesh.position.y,
										this.mesh.position.z);
	if (this.meshPostion.x < 8)
		this.mesh.position.x += cubeSize;
	else
		this.mesh.position.x = 0;
}

Block.prototype.shiftUp = function()
{
	this.meshPostion = game.meshToGrid(	this.mesh.position.x,
										this.mesh.position.y+cubeSize,
										this.mesh.position.z);
	if (this.meshPostion.y < 8)
		this.mesh.position.y += cubeSize;
	else
		this.mesh.position.y = 0;
}

Block.prototype.color = 0x0c008f;

