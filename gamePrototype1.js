var cubeSize = 50;

var ShiftEscape = function ()
{
	//canvas size
	this.width = 600;
	this.height = 500;

	// Main components of a scene
	this.scene = null;
	this.camera = null;
	this.renderer = null;

	// camera view
	this.camView = 'top';

	// light
	this.light = null;
	this.ambientLight = null;

	// axises
	this.axis = null;

	//Game vars
	this.animID = null;
	this.floorGrid = new Create2DObj(8);
	this.gameGrid = new Create2DObj(8);
	this.blockParent = new THREE.Mesh(new THREE.Geometry());

	this.hilighted = new Position(0, 0, 0);

	this.level = 1;
	this.levels = 5;

	this.doExplode = false;
	this.timeoutID = null;

	this.textYa = null;
}

ShiftEscape.prototype.numOfShifts = 10;

ShiftEscape.prototype.init = function()
{
	var that = this;
	// To make three.js work we need three things,
	// a Scene
	this.scene = new THREE.Scene();

	// a camera
	// (field of view, aspect ratio, near and far clipping plane )
	this.camera = new THREE.PerspectiveCamera
							( 75, this.width / this.height, 0.1, 1000 );

	// and a renderer
	this.renderer = new THREE.WebGLRenderer();
	this.renderer.setSize( this.width, this.height );
	document.body.appendChild( this.renderer.domElement );

	// enable shadows on the renderer
	this.renderer.shadowMapEnabled = true;

	// enable shadows for an object
	// this.addShadowToObj(this.robot);

	this.renderer.setClearColor(0xeeeeee, 1.0);

	this.camera.position.x = 175;
	this.camera.position.y = -30;
	this.camera.position.z = 310;
	this.camera.rotation.x = 0.5;
	this.camera.rotation.y = 0;
	this.camera.rotation.z = 0;

	// add Axis
	// this.axis = new THREE.AxisHelper(500);
	// this.scene.add(this.axis); 

	this.createSkyBox();

	// create obj play with robot and add to scene
	this.scene.add( this.blockParent );

	if (! this.levelBuilder())
	{
		this.robot = new Robot();
		this.scene.add( this.robot.cubeMesh );

		this.gameGrid[1][2] = new Block( 1, 2 );
		this.scene.add(this.gameGrid[1][2].mesh);

		this.gameGrid[2][2] = new Block( 2, 2 );
		this.scene.add(this.gameGrid[2][2].mesh);
	}

	var floor = this.createMainPlane(0, 0, -10);
	this.scene.add( floor );
	this.addShadowToObj(floor);

	for (var i = 0; i < 8; ++i) 
	{
		for (var j = 0; j < 8; ++j) 
		{
			this.floorGrid[j][i] = new Plane(j*cubeSize, i*cubeSize, 0);
			this.scene.add( this.floorGrid[j][i] );
			this.addShadowToObj(this.floorGrid[j][i]);
		}
	}
		
	this.highlightFloorRow(this.hilighted.y);
	this.highlightFloorColumn(this.hilighted.x);


	// create light
	this.light = this.createLight(this.floorGrid[1][1]);
	this.scene.add(this.light);
	this.light = this.createLight(this.floorGrid[1][6]);
	this.scene.add(this.light);
	this.light = this.createLight(this.floorGrid[6][1]);
	this.scene.add(this.light);
	this.light = this.createLight(this.floorGrid[6][6]);
	this.scene.add(this.light);

	this.ambientLight = new THREE.AmbientLight(0x202020);
	this.scene.add(this.ambientLight);

	//Add text
	this.textYa = document.createElement('div');
	this.textYa.style.position = 'absolute';
	this.textYa.style.width = 110;
	this.textYa.style.height = 50;
	this.textYa.style.backgroundColor = "white";
	this.textYa.innerHTML = "<h2>Shifts: " + this.numOfShifts + "</h2>";
	this.textYa.style.top = 0 + 'px';
	this.textYa.style.left = 420 + 'px';
	document.body.appendChild(this.textYa);

	var instructions = document.createElement('div');
	instructions.style.position = 'absolute';
	instructions.style.backgroundColor = "white";
	instructions.innerHTML = 	"<h2> How to play: </h2>" +
								"Get to the top of the map using:<br>" +
								"WASD to move the robot<br>" +
								"The arrow keys to move the highlight<br>" +
								"V is to change the camera view<br>" +
								"Space is to shift the row<br>" +
								"Enter is to shift the column and <br>" +
								"R is to retry the level";

	instructions.style.top = 520 + 'px';
	instructions.style.left = 420 + 'px';
	document.body.appendChild(instructions);


	this.keys = {};

	document.addEventListener
	('keydown', function(event) 
	{
		if (that.keys[event.keyCode] !== 'triggered')
		{
			that.keys[event.keyCode] = true;
		}
	});

	document.addEventListener
	('keyup', function(event) 
	{
		that.keys[event.keyCode] = false;
	});

	this.animate();
}

ShiftEscape.prototype.animate = function()
{
	var that = this;

	var loop = function()
	{
		if (that.doExplode) that.robot.explode();
		that.renderer.render(that.scene, that.camera);
		that.handleInput();
	    this.animID = requestAnimationFrame(loop, that.renderer.domElement);
	}; 
	loop(); 
}

ShiftEscape.prototype.levelBuilder = function()
{
	var that = this;
	var oReq = new XMLHttpRequest();
	oReq.onload = reqListener;
	oReq.open("get", "levels/level" + this.level, true);
	oReq.send();
	return true;

	function reqListener ()
	{
		// console.log(this.responseText);

		var splitReturn = this.responseText.split('\n');
		var count = 7;

		var splitSpace = null;
		for ( var i = 0; i < 8 ; ++i )
		{
			splitSpace = splitReturn[count].split(' ');

			// console.log(splitSpace);

			for ( var j = 0; j < 8 ; ++j )
			{
				switch( splitSpace[j] )
				{
					case 'x':
						that.gameGrid[j][i] = new Block( j, i );
						that.blockParent.add( that.gameGrid[j][i].mesh );
						break;

					case 'o':
						that.robot = new Robot( j, i );
						that.blockParent.add( that.robot.cubeMesh );
						break;

				}
			}

			--count;
		}

		// console.log(obj2D);
		// console.log(obj2D[0][0]);
	};
}

// <----------------- Highlight functions ------------------------------------->

ShiftEscape.prototype.moveHighlight = function(x, y)
{
	var dx = this.hilighted.x + (x || 0);
	var dy = this.hilighted.y + (y || 0);

	if ( dx >= 0 && dx <= 7 &&
		 dy >= 0 && dy <= 7 )
	{
		this.hilighted.x = dx;
		this.hilighted.y = dy;
		return true;
	}
	return false;
}

ShiftEscape.prototype.highlightFloorRow = function(row)
{
	var currItem = null;
	for (var i = 0; i < 8; i++)
	{
		currItem = this.floorGrid[i][row];
		if ( currItem != undefined )
		{
			currItem.material.color.setHex(Plane.prototype.highLightColor);
		}
	}	
}

ShiftEscape.prototype.unHighlightFloorRow = function(row, column)
{
	var currItem = null;
	for (var i = 0; i < 8; i++)
	{
		currItem = this.floorGrid[i][row];
		if (currItem != undefined && i != column)
		{
			currItem.material.color.setHex(Plane.prototype.mainColor);
		}
	}	
}

ShiftEscape.prototype.highlightFloorColumn = function(Column)
{
	var currItem = null;
	for (var i = 0; i < 8; i++)
	{
		currItem = this.floorGrid[Column][i];
		if (currItem != undefined)
		{
			currItem.material.color.setHex(Plane.prototype.highLightColor);
		}
	}
}

ShiftEscape.prototype.unHighlightFloorColumn = function(Column, row)
{
	var currItem = null;
	for (var i = 0; i < 8; i++)
	{
		currItem = this.floorGrid[Column][i];
		if (currItem != undefined && i != row)
		{
			currItem.material.color.setHex(Plane.prototype.mainColor);
		}
	}
}

// <---------------------- Game functions ------------------------------------->

ShiftEscape.prototype.gameOver = function()
{
	var that = this;

	var callGame = function()
	{
		alert('Can not shift in the same row or column as robot!');
		cancelAnimationFrame(that.animID);
		that.reInit(false);
	}
	this.timeoutID = setTimeout(callGame, 3000);
}

ShiftEscape.prototype.reInit = function(nextLevel)
{
	this.scene.remove(this.blockParent);
	this.blockParent = new THREE.Mesh(new THREE.Geometry());
	this.scene.add(this.blockParent);
	this.doExplode = false;	
	this.gameGrid = new Create2DObj(8);
	if (nextLevel && this.level+1 < this.levels) ++this.level;
	this.levelBuilder();
	this.unHighlightFloorRow(this.hilighted.y);
	this.unHighlightFloorColumn(this.hilighted.x);
	this.hilighted = new Position(0, 0, 0);
	this.highlightFloorRow(this.hilighted.y);
	this.highlightFloorColumn(this.hilighted.x);
	this.numOfShifts = 10;
	this.textYa.innerHTML = "<h2>Shifts: " + this.numOfShifts + "</h2>";


}

ShiftEscape.prototype.shiftRow = function(row)
{
	// when space is pressed
	if ( (this.robot.cubeMesh.position.y/cubeSize) == row )
	{
		this.doExplode = true;
		this.gameOver();
		return;
	}
	var currItem = null;
	var nextItem = this.gameGrid[0][row];
	for (var i = 0; i < 8; i++)
	{
		currItem = nextItem;
		nextItem = this.gameGrid[(i+1)%8][row];
		if (currItem != undefined)
		{
			currItem.shiftRight();
		}

		this.gameGrid[(i+1)%8][row] = currItem;
	}

	--this.numOfShifts;
	this.textYa.innerHTML = "<h2>Shifts: " + this.numOfShifts + "</h2>";
}

ShiftEscape.prototype.shiftColumn = function(column)
{
	// when enter is pressed
	if ( (this.robot.cubeMesh.position.x/cubeSize) == column )
	{
		this.doExplode = true;
		this.gameOver();
		return;
	}
	var currItem = null;
	var nextItem = this.gameGrid[column][0];
	for (var i = 0; i < 8; i++)
	{
		currItem = nextItem;
		nextItem = this.gameGrid[column][(i+1)%8];
		if (currItem != undefined)
		{
			currItem.shiftUp();
		}

		this.gameGrid[column][(i+1)%8] = currItem;
	}
	
	--this.numOfShifts;
	this.textYa.innerHTML = "<h2>Shifts: " + this.numOfShifts + "</h2>";
}

ShiftEscape.prototype.meshToGrid = function(x, y, z)
{
	return new Position(x/cubeSize, y/cubeSize, z);
}

ShiftEscape.prototype.gridToMesh = function(x, y)
{
	return new Position(x*cubeSize, y*cubeSize, 0);
}

// <-------------------- Create functions ------------------------------------->

ShiftEscape.prototype.createMainPlane = function (x, y , z)
{
	var geometry = new THREE.PlaneGeometry(cubeSize*8 , cubeSize*8 , 10, 10);
	var material= new THREE.MeshLambertMaterial({color: 0x000000});
	var plane = new THREE.Mesh(geometry, material);
	plane.position.x += cubeSize*3.5;
	plane.position.y += cubeSize*3.5;
	plane.position.z = z;
	return plane;
}

ShiftEscape.prototype.createLight = function (mesh)
{
	var light = new THREE.SpotLight( 0xffffff, .75 );
	light.position.copy( mesh.position );
	light.position.z = 300;

	light.castShadow = true;
	// light.shadowCameraVisible = true;
	
	light.target = mesh;

	// enable shadows for a light
	light.castShadow = true;

	// light.angle = .5;

	return light;
}

ShiftEscape.prototype.createSkyBox = function()
{
	// var imagePrefix = "assets/wall-";
	// var directions  = ["xpos", "xneg", "ypos", "yneg", "zpos", "zneg"];
	var imagePrefix = "assets/pic";
	var imageSuffix = ".jpg";
	var skyGeometry = new THREE.CubeGeometry( 1000, 1000, 1000 );	
	
	var materialArray = [];
	for (var i = 0; i < 6; i++)
		materialArray.push( new THREE.MeshBasicMaterial({
			map: THREE.ImageUtils.loadTexture( imagePrefix + i + imageSuffix ),
			side: THREE.BackSide
		}));
	var skyMaterial = new THREE.MeshFaceMaterial( materialArray );
	var skyBox = new THREE.Mesh( skyGeometry, skyMaterial );
	skyBox.position.set(100, 100, 0);
	this.scene.add( skyBox );
}

ShiftEscape.prototype.addShadowToObj = function(mesh)
{
	// enable shadows for an mesh
	mesh.castShadow = true;
	mesh.receiveShadow = true;
}

// <---------------------- Input function ------------------------------------->

ShiftEscape.prototype.handleInput = function()
{
	if(this.keys[37] === true)
	{
		this.keys[37] = 'triggered';
		//console.log('Left was pressed');
		if (cToggle)
		{
			this.camera.position.x -= 10;
			console.log('this.camera x: ' + this.camera.position.x +
						' y: ' + this.camera.position.y +
						' z: ' + this.camera.position.z);
		}
		else
		{
			if(this.moveHighlight(-1, 0))
			{
				this.highlightFloorColumn(this.hilighted.x);
				this.unHighlightFloorColumn(this.hilighted.x+1,
											this.hilighted.y);
			}
		}
	}

	if(this.keys[39] === true)
	{
		this.keys[39] = 'triggered';
		//console.log('Right was pressed');
		if (cToggle)
		{
			this.camera.position.x += 10;
			console.log('this.camera x: ' + this.camera.position.x +
						' y: ' + this.camera.position.y +
						' z: ' + this.camera.position.z);
		}
		else
		{
			if(this.moveHighlight(1, 0))
			{
				this.highlightFloorColumn(this.hilighted.x);
				this.unHighlightFloorColumn(this.hilighted.x-1,
											this.hilighted.y);
			}
		}
	}

	if(this.keys[38] === true)
	{
		this.keys[38] = 'triggered';
		//console.log('Up was pressed');
		if (cToggle)
		{
			this.camera.position.y += 10;
			console.log('this.camera x: ' + this.camera.position.x +
						' y: ' + this.camera.position.y +
						' z: ' + this.camera.position.z);
		}
		else
		{
			if(this.moveHighlight(0, 1))
			{
				this.highlightFloorRow(this.hilighted.y);
				this.unHighlightFloorRow(this.hilighted.y-1, this.hilighted.x);
			}
		}
	}

	if(this.keys[40] === true)  
	{
		this.keys[40] = 'triggered';
		//console.log('Down was pressed');
		if (cToggle)
		{
			this.camera.position.y -= 10;
			console.log('this.camera x: ' + this.camera.position.x +
							' y: ' + this.camera.position.y +
							' z: ' + this.camera.position.z);
		}
		else
		{
			if(this.moveHighlight(0, -1))
			{
				this.highlightFloorRow(this.hilighted.y);
				this.unHighlightFloorRow(this.hilighted.y+1, this.hilighted.x);
			}
		}
	}

	if(this.keys[65] === true)
	{
		this.keys[65] = 'triggered';
		//console.log('A was pressed');
		this.robot.move(-1, 0);
	}
	
	if(this.keys[68] === true)
	{
		this.keys[68] = 'triggered';
		//console.log('D was pressed');
		this.robot.move(1, 0);

	}
	
	if(this.keys[82] === true)
	{
		this.keys[82] = 'triggered';
		//console.log('R was pressed');
		if (this.timeoutID != null)
			clearTimeout(this.timeoutID);
		game.reInit(false);
		// this.robot.rotation.z -= .1;
	}
	
	if(this.keys[87] === true)
	{
		this.keys[87] = 'triggered';
		//console.log('W was pressed');
		if (cToggle)
		{
			this.camera.position.z += 10;
			console.log('this.camera x: ' + this.camera.position.x +
						' y: ' + this.camera.position.y +
						' z: ' + this.camera.position.z);
		}
		else
		{
			// this.robot.cubeMesh.position.z += cubeSize/2;
			this.robot.move(0, 1);
		}
	}
	
	if(this.keys[83] === true)
	{
		this.keys[83] = 'triggered';
		//console.log('S was pressed');
		if (cToggle)
		{
			this.camera.position.z -= 10;
			console.log('this.camera x: ' + this.camera.position.x +
						' y: ' + this.camera.position.y +
						' z: ' + this.camera.position.z);
		}
		else
		{
			// this.robot.cubeMesh.position.z -= cubeSize/2;
			this.robot.move(0, -1);
		}
	}

	if(this.keys[32] === true) 
	{
		this.keys[32] = 'triggered';
		//console.log('Space was pressed');
		// if (this.numOfShifts != 0)
		if (true)
		{
			this.shiftRow(this.hilighted.y);
		}
		else
		{
			alert("out of shifts");
		}
	}

	if(this.keys[13] === true) 
	{
		this.keys[13] = 'triggered';
		//console.log('Enter was pressed');
		// if (this.numOfShifts != 0)
		if (true)
		{
			this.shiftColumn(this.hilighted.x);
		}
		else
		{
			alert("out of shifts");
		}
	}

	if(this.keys[86] === true) 
	{
		this.keys[86] = 'triggered';
		//console.log('V was pressed');
		if (this.camView == 'front')
		{
			this.camera.position.x = 175;
			this.camera.position.y = 175;
			this.camera.position.z = 350;
			this.camera.rotation.x = 0;
			this.camera.rotation.y = 0;
			this.camera.rotation.z = 0;
			this.camView = 'top';
		}
		else
		{
			this.camera.position.x = 175;
			this.camera.position.y = -30;
			this.camera.position.z = 310;
			this.camera.rotation.x = 0.5;
			this.camera.rotation.y = 0;
			this.camera.rotation.z = 0;
			this.camView = 'front';
		}
	}
}

// <--------------------- Other functions ------------------------------------->

//Creating a 2D obj function
function Create2DObj(rows) 
{
  var obj = {};

  for (var i = 0; i < rows; i++) 
  {
     obj[i] = {};
  }

  return obj;
}


// <------------------------- mouse/button events ----------------------------->

document.addEventListener
('keydown', function(event) 
{
	// console.log(event.keyCode);

	// if(event.keyCode == 74) 
	// {
	// 	//console.log('J was pressed');
	// 	game.camera.lookAt(game.robot.cubeMesh.position);

	// }

	if(event.keyCode == 91)
	{
		// console.log('command is down');
		cmdDown = true;
	}

	if(event.keyCode == 67)
	{
		//console.log('C was pressed');
		cToggle = !cToggle;
	}

});

document.addEventListener
('keyup', function(event) 
{
	// console.log(event.keyCode);

	if(event.keyCode == 91)
	{
		// console.log('command is up');
		cmdDown = false;
	}

});

// control camera rotations
var down = false;
var sx, sy;
var cmdDown = false;
var cToggle = false;
window.onmousedown = function (ev)
{
	down = true; sx = ev.clientX; sy = ev.clientY;
};

window.onmouseup = function(){ down = false; };

window.onmousemove = function(ev)
{
	if (down && cToggle)
	{
		var dx = ev.clientX - sx;
		var dy = ev.clientY - sy;

		if (cmdDown)
		{
			game.camera.rotation.z += dx*0.01;
		}
		else
		{
			game.camera.rotation.y -= dx*0.01;
			game.camera.rotation.x -= dy*0.01;
		}
		console.log('camera.rotation x: ' + game.camera.rotation.x +
					' y: ' + game.camera.rotation.y +
					' z: ' + game.camera.rotation.z );
		sx += dx;
		sy += dy;
	}
}



